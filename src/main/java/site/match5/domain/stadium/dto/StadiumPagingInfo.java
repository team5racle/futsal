package site.match5.domain.stadium.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StadiumPagingInfo {
    private Integer totalItem;
    private Integer totalPage;
}
